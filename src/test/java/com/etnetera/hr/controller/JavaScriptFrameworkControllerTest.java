package com.etnetera.hr.controller;

import com.etnetera.hr.TestUtils;
import com.etnetera.hr.data.FrameworkFilter;
import com.etnetera.hr.data.HypeLevel;
import com.etnetera.hr.data.JavaScriptFramework;
import com.etnetera.hr.data.Messages;
import com.etnetera.hr.repository.JavaScriptFrameworkRepository;
import com.etnetera.hr.service.JavaScriptFrameworkService;
import com.etnetera.hr.service.JavaScriptFrameworkServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = JavaScriptFrameworkController.class)
@Import(JavaScriptFrameworkServiceImpl.class)
public class JavaScriptFrameworkControllerTest {

    private final String CONTENT_TYPE = "application/json";
    private final String BASIC_URI = "/frameworks";
    private final String FILTER_URI = BASIC_URI + "/filter";
    private final String URI_ID_PARAMETER = BASIC_URI + "/{id}";

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    private JavaScriptFrameworkService service;

    @MockBean
    private JavaScriptFrameworkRepository repository;

    @Test
    public void testGetAllFrameworks() throws Exception {
        JavaScriptFramework testFramework = new JavaScriptFramework("Test3", 3,
                LocalDate.of(2002, 12, 12), HypeLevel.MEDIUM);
        Mockito.when(repository.findAll()).thenReturn(List.of(testFramework));

        MvcResult result = mockMvc.perform((get(BASIC_URI))
                .contentType(CONTENT_TYPE))
                .andExpect(status().isOk())
                .andReturn();
        String actualResponseBody = result.getResponse().getContentAsString();

        assertThat(actualResponseBody).isEqualToIgnoringWhitespace(objectMapper.writeValueAsString(List.of(testFramework)));
    }

    @Test
    public void testGetFrameworkById() throws Exception {
        JavaScriptFramework testFramework = new JavaScriptFramework(3L, "Test3", 3,
                LocalDate.of(2002, 12, 12), HypeLevel.MEDIUM);
        Mockito.when(repository.findById(3L)).thenReturn(Optional.of(testFramework));

        MvcResult result = mockMvc.perform((get(URI_ID_PARAMETER, 3))
                .contentType(CONTENT_TYPE))
                .andExpect(status().isOk())
                .andReturn();
        String actualResponseBody = result.getResponse().getContentAsString();

        assertThat(actualResponseBody).isEqualToIgnoringWhitespace(objectMapper.writeValueAsString(testFramework));
    }

    @Test
    public void testGetFrameworkByIdNotFound() throws Exception {
        Mockito.when(repository.findById(any())).thenReturn(Optional.empty());

        MvcResult result = mockMvc.perform((get(URI_ID_PARAMETER, 1))
                .contentType(CONTENT_TYPE))
                .andExpect(status().isNotFound())
                .andReturn();
        String actualResponseBody = result.getResponse().getContentAsString();

        assertThat(actualResponseBody).isEqualToIgnoringWhitespace(Messages.NOT_FOUND_MESSAGE);
    }

    @Test
    public void testGetFrameworkByFilter_Name() throws Exception {
        JavaScriptFramework testFramework = new JavaScriptFramework(3L, "Test3", 3,
                LocalDate.of(2002, 12, 12), HypeLevel.MEDIUM);
        Mockito.when(repository.findByName(any())).thenReturn(Optional.of(testFramework));

        FrameworkFilter filter = new FrameworkFilter("Test3", null, null, null);

        MvcResult result = mockMvc.perform((post(FILTER_URI))
                .contentType(CONTENT_TYPE)
                .content(objectMapper.writeValueAsString(filter)))
                .andExpect(status().isOk())
                .andReturn();
        String actualResponseBody = result.getResponse().getContentAsString();

        assertThat(actualResponseBody).isEqualToIgnoringWhitespace(objectMapper.writeValueAsString(testFramework));
    }

    @Test
    public void testGetFrameworkByFilter_HypeLevel() throws Exception {
        List testData = TestUtils.getTestData()
                .stream()
                .filter(javaScriptFramework -> javaScriptFramework.getHypeLevel() == HypeLevel.HIGH)
                .collect(Collectors.toList());

        Mockito.when(repository.findByHypeLevel(any())).thenReturn(testData);

        FrameworkFilter filter = new FrameworkFilter(null, HypeLevel.HIGH, null, null);

        MvcResult result = mockMvc.perform((post(FILTER_URI))
                .contentType(CONTENT_TYPE)
                .content(objectMapper.writeValueAsString(filter)))
                .andExpect(status().isOk())
                .andReturn();
        String actualResponseBody = result.getResponse().getContentAsString();

        assertThat(actualResponseBody).isEqualToIgnoringWhitespace(objectMapper.writeValueAsString(testData));
    }

    @Test
    public void testGetFrameworkByFilter_DeprecationTimeBefore() throws Exception {
        LocalDate testDate = LocalDate.of(2001, 12, 12);

        List testData = TestUtils.getTestData()
                .stream()
                .filter(javaScriptFramework -> javaScriptFramework.getDeprecationDate().isBefore(testDate))
                .collect(Collectors.toList());

        Mockito.when(repository.findByDeprecationDateIsBefore(any())).thenReturn(testData);

        FrameworkFilter filter = new FrameworkFilter(null, null, testDate, null);

        MvcResult result = mockMvc.perform((post(FILTER_URI))
                .contentType(CONTENT_TYPE)
                .content(objectMapper.writeValueAsString(filter)))
                .andExpect(status().isOk())
                .andReturn();
        String actualResponseBody = result.getResponse().getContentAsString();

        assertThat(actualResponseBody).isEqualToIgnoringWhitespace(objectMapper.writeValueAsString(testData));
    }

    @Test
    public void testGetFrameworkByFilter_DeprecationTimeAfter() throws Exception {
        LocalDate testDate = LocalDate.of(2000, 12, 12);

        List testData = TestUtils.getTestData()
                .stream()
                .filter(javaScriptFramework -> javaScriptFramework.getDeprecationDate().isAfter(testDate))
                .collect(Collectors.toList());

        Mockito.when(repository.findByDeprecationDateIsAfter(any())).thenReturn(testData);

        FrameworkFilter filter = new FrameworkFilter(null, null, null, testDate);

        MvcResult result = mockMvc.perform((post(FILTER_URI))
                .contentType(CONTENT_TYPE)
                .content(objectMapper.writeValueAsString(filter)))
                .andExpect(status().isOk())
                .andReturn();
        String actualResponseBody = result.getResponse().getContentAsString();

        assertThat(actualResponseBody).isEqualToIgnoringWhitespace(objectMapper.writeValueAsString(testData));
    }

    @Test
    public void testGetFrameworkByFilter_BadRequest() throws Exception {
        FrameworkFilter filter = new FrameworkFilter(null, null, null, null);

        MvcResult result = mockMvc.perform((post(FILTER_URI))
                .contentType(CONTENT_TYPE)
                .content(objectMapper.writeValueAsString(filter)))
                .andExpect(status().isBadRequest())
                .andReturn();
        String actualResponseBody = result.getResponse().getContentAsString();

        assertThat(actualResponseBody).isEqualToIgnoringWhitespace(Messages.NO_FILTER);
    }

    @Test
    public void testCreateFramework() throws Exception {
        JavaScriptFramework testFrameworkToSave = new JavaScriptFramework("Test", 1,
                LocalDate.of(2002, 12, 12), HypeLevel.MEDIUM);
        JavaScriptFramework savedFramework = new JavaScriptFramework(1L, "Test", 1,
                LocalDate.of(2002, 12, 12), HypeLevel.MEDIUM);
        Mockito.when(repository.save(any())).thenReturn(savedFramework);

        MvcResult result = mockMvc.perform((post(BASIC_URI))
                .contentType(CONTENT_TYPE)
                .content(objectMapper.writeValueAsString(testFrameworkToSave)))
                .andExpect(status().isOk())
                .andReturn();
        String actualResponseBody = result.getResponse().getContentAsString();

        assertThat(actualResponseBody).isEqualToIgnoringWhitespace(objectMapper.writeValueAsString(savedFramework));
    }

    @Test
    public void testDeleteFramework() throws Exception {
        JavaScriptFramework testFramework = new JavaScriptFramework(1L, "Test1", 1,
                LocalDate.of(2002, 12, 12), HypeLevel.MEDIUM);
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(testFramework));

        MvcResult result = mockMvc.perform((delete(URI_ID_PARAMETER, 1))
                .contentType(CONTENT_TYPE))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void testDeleteFrameworkNotFound() throws Exception {
        Mockito.when(repository.findById(1L)).thenReturn(Optional.empty());

        MvcResult result = mockMvc.perform((delete(URI_ID_PARAMETER, 1))
                .contentType(CONTENT_TYPE))
                .andExpect(status().isNotFound())
                .andReturn();
        String actualResponseBody = result.getResponse().getContentAsString();

        assertThat(actualResponseBody).isEqualToIgnoringWhitespace(Messages.NOT_FOUND_MESSAGE);
    }

    @Test
    public void testUpdateFrameworkNotFound() throws Exception {
        Mockito.when(repository.findById(any())).thenReturn(Optional.empty());

        MvcResult result = mockMvc.perform((put(URI_ID_PARAMETER, 1))
                .contentType(CONTENT_TYPE)
                .content(objectMapper.writeValueAsString(new JavaScriptFramework())))
                .andExpect(status().isNotFound())
                .andReturn();
        String actualResponseBody = result.getResponse().getContentAsString();

        assertThat(actualResponseBody).isEqualToIgnoringWhitespace(Messages.NOT_FOUND_MESSAGE);
    }

    @Test
    public void testUpdateFramework() throws Exception {
        JavaScriptFramework testFramework1 = new JavaScriptFramework(1L, "Test1", 1,
                LocalDate.of(2000, 12, 12), HypeLevel.LOW);

        JavaScriptFramework testFramework2 = new JavaScriptFramework("Test2", 2,
                LocalDate.of(2001, 12, 12), HypeLevel.LOW);

        Mockito.when(repository.findById(any())).thenReturn(Optional.of(testFramework1));

        testFramework1.setName(testFramework2.getName());
        testFramework1.setVersion(testFramework2.getVersion());
        testFramework1.setDeprecationDate(testFramework2.getDeprecationDate());
        testFramework1.setHypeLevel(testFramework2.getHypeLevel());

        Mockito.when(repository.save(any())).thenReturn(testFramework1);

        MvcResult result = mockMvc.perform((put(URI_ID_PARAMETER, 1))
                .contentType(CONTENT_TYPE)
                .content(objectMapper.writeValueAsString(testFramework2)))
                .andExpect(status().isOk())
                .andReturn();
        String actualResponseBody = result.getResponse().getContentAsString();

        assertThat(actualResponseBody).isEqualToIgnoringWhitespace(objectMapper.writeValueAsString(testFramework1));
    }
}
