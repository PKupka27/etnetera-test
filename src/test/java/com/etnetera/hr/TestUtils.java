package com.etnetera.hr;

import com.etnetera.hr.data.HypeLevel;
import com.etnetera.hr.data.JavaScriptFramework;

import java.time.LocalDate;
import java.util.List;

public class TestUtils {

    public static List<JavaScriptFramework> getTestData() {
        JavaScriptFramework testFramework1 = new JavaScriptFramework("Test1", 1,
                LocalDate.of(2000, 12, 12), HypeLevel.LOW);

        JavaScriptFramework testFramework2 = new JavaScriptFramework("Test2", 2,
                LocalDate.of(2001, 12, 12), HypeLevel.LOW);

        JavaScriptFramework testFramework3 = new JavaScriptFramework("Test3", 3,
                LocalDate.of(2002, 12, 12), HypeLevel.MEDIUM);

        JavaScriptFramework testFramework4 = new JavaScriptFramework("Test4", 4,
                LocalDate.of(2003, 12, 12), HypeLevel.HIGH);

        JavaScriptFramework testFramework5 = new JavaScriptFramework("Test5", 5,
                LocalDate.of(2004, 12, 12), HypeLevel.HIGH);
        return List.of(testFramework1, testFramework2, testFramework3, testFramework4, testFramework5);
    }

}
