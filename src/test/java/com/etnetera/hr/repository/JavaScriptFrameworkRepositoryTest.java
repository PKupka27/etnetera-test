package com.etnetera.hr.repository;

import com.etnetera.hr.TestUtils;
import com.etnetera.hr.data.HypeLevel;
import com.etnetera.hr.data.JavaScriptFramework;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@DataJpaTest
@RunWith(SpringRunner.class)
public class JavaScriptFrameworkRepositoryTest {

    @Autowired
    JavaScriptFrameworkRepository repository;

    @Test
    public void tesFindFrameworkByDeprecationDateBefore() {
        createData();
        List<JavaScriptFramework> result = repository.findByDeprecationDateIsBefore(LocalDate.of(2002, 12, 12));

        Assertions.assertTrue(result.size() < 3);
        Assertions.assertTrue(result.size() > 1);
    }

    @Test
    public void tesFindFrameworkByDeprecationDateBeforeIsEmpty() {
        createData();
        List<JavaScriptFramework> result = repository.findByDeprecationDateIsBefore(LocalDate.of(1999, 12, 12));

        Assertions.assertTrue(result.isEmpty());
    }

    @Test
    public void tesFindFrameworkByDeprecationDateAfter() {
        createData();
        List<JavaScriptFramework> result = repository.findByDeprecationDateIsAfter(LocalDate.of(2002, 12, 12));

        Assertions.assertTrue(result.size() < 3);
        Assertions.assertTrue(result.size() > 1);
    }

    @Test
    public void tesFindFrameworkByDeprecationDateAfterIsEmpty() {
        createData();
        List<JavaScriptFramework> result = repository.findByDeprecationDateIsAfter(LocalDate.of(2020, 12, 12));

        Assertions.assertTrue(result.isEmpty());
    }

    @Test
    public void tesFindFrameworkByName() {
        createData();
        Optional<JavaScriptFramework> result = repository.findByName("Test1");

        Assertions.assertTrue(result.isPresent());
        Assertions.assertTrue(Objects.nonNull(result.get().getDeprecationDate()));
        Assertions.assertTrue(result.get().getHypeLevel() == HypeLevel.LOW);
    }

    @Test
    public void tesFindFrameworkByHypeLevel() {
        createData();
        List<JavaScriptFramework> result = repository.findByHypeLevel(HypeLevel.HIGH);

        Assertions.assertTrue(!result.isEmpty());
    }

    private void createData() {
        repository.saveAll(TestUtils.getTestData());
    }
}
