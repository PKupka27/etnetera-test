package com.etnetera.hr.service;

import com.etnetera.hr.TestUtils;
import com.etnetera.hr.data.ApiException;
import com.etnetera.hr.data.FrameworkFilter;
import com.etnetera.hr.data.HypeLevel;
import com.etnetera.hr.data.JavaScriptFramework;
import com.etnetera.hr.repository.JavaScriptFrameworkRepository;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.mockito.ArgumentMatchers.any;

@RunWith(MockitoJUnitRunner.class)
public class JavaScriptFrameworkServiceTest {

    @Mock
    JavaScriptFrameworkRepository repository;

    @InjectMocks
    JavaScriptFrameworkServiceImpl service;

    @Test
    public void testGetAllFrameworks() {
        Mockito.when(repository.findAll()).thenReturn(TestUtils.getTestData());

        List<JavaScriptFramework> result =
                StreamSupport.stream(service.getAllFrameworks().spliterator(), false)
                        .collect(Collectors.toList());

        Assertions.assertTrue(!result.isEmpty());
    }

    @Test
    public void testGetFrameworkByIdSuccess() {
        Mockito.when(repository.findById(any())).thenReturn(Optional.of(TestUtils.getTestData().get(0)));

        JavaScriptFramework result = service.getFrameworkById(1L);

        Assertions.assertTrue(Objects.nonNull(result.getName()));
        Assertions.assertTrue(Objects.nonNull(result.getHypeLevel()));
        Assertions.assertTrue(Objects.nonNull(result.getVersion()));
    }

    @Test
    public void testGetFrameworkByIdThrowsException() {
        Mockito.when(repository.findById(any())).thenReturn(Optional.empty());

        ApiException result = Assertions.assertThrows(ApiException.class, () ->
                service.getFrameworkById(1L)
        );

        Assertions.assertEquals(result.getMessage(), "Framework not found");
        Assertions.assertEquals(result.getStatus(), HttpStatus.NOT_FOUND);
    }

    @Test
    public void testDeleteFrameworkSuccess() {
        Mockito.when(repository.findById(any())).thenReturn(Optional.of(TestUtils.getTestData().get(0)));
        Assertions.assertDoesNotThrow(() -> service.deleteFramework(1L));
    }

    @Test
    public void testDeleteFrameworkThrowsException() {
        Mockito.when(repository.findById(any())).thenReturn(Optional.empty());

        ApiException result = Assertions.assertThrows(ApiException.class, () ->
                service.getFrameworkById(1L)
        );

        Assertions.assertEquals(result.getMessage(), "Framework not found");
        Assertions.assertEquals(result.getStatus(), HttpStatus.NOT_FOUND);
    }

    @Test
    public void testUpdateFrameworkSuccess() {
        JavaScriptFramework testFramework1 = new JavaScriptFramework(1L,"Test1", 1,
                LocalDate.of(2000, 12, 12), HypeLevel.LOW);

        JavaScriptFramework testFramework2 = new JavaScriptFramework( "Test2", 2,
                LocalDate.of(2001, 12, 12), HypeLevel.LOW);

        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(testFramework1));

        testFramework1.setName(testFramework2.getName());
        testFramework1.setVersion(testFramework2.getVersion());
        testFramework1.setDeprecationDate(testFramework2.getDeprecationDate());
        testFramework1.setHypeLevel(testFramework2.getHypeLevel());

        Mockito.when(repository.save(any())).thenReturn(testFramework1);

        JavaScriptFramework result = service.updateFramework(1L, testFramework2);

        Assertions.assertNotNull(result);
        Assertions.assertEquals(testFramework2.getName(), result.getName());
        Assertions.assertEquals(testFramework2.getVersion(), result.getVersion());
        Assertions.assertEquals(testFramework2.getDeprecationDate(), result.getDeprecationDate());
        Assertions.assertEquals(testFramework2.getHypeLevel(), result.getHypeLevel());
    }

    @Test
    public void testUpdateFrameworkThrowsException() {
        Mockito.when(repository.findById(any())).thenReturn(Optional.empty());

        ApiException result = Assertions.assertThrows(ApiException.class, () ->
                service.getFrameworkById(1L)
        );

        Assertions.assertEquals(result.getMessage(), "Framework not found");
        Assertions.assertEquals(result.getStatus(), HttpStatus.NOT_FOUND);
    }

    @Test
    public void testCreateFrameworkFrameworkSuccess() {
        JavaScriptFramework createdTestFramework = TestUtils.getTestData().get(0);
        createdTestFramework.setId(1L);

        Mockito.when(repository.save(any())).thenReturn(createdTestFramework);

        JavaScriptFramework result = service.createFramework(TestUtils.getTestData().get(0));

        Assertions.assertNotNull(result);
        Assertions.assertNotNull(result.getId());
        Assertions.assertNotNull(result.getHypeLevel());
        Assertions.assertNotNull(result.getVersion());
        Assertions.assertNotNull(result.getDeprecationDate());
    }

    @Test
    public void testGetFrameworkByFilter_Name() {
        Mockito.when(repository.findByName(any())).thenReturn(Optional.of(TestUtils.getTestData().get(0)));

        FrameworkFilter filter = new FrameworkFilter("Test1", null, null,null);

        Object result = service.getFrameworkByFilter(filter);

        Assertions.assertTrue(result instanceof JavaScriptFramework);
    }

    @Test
    public void testGetFrameworkByFilter_NameThrowsException() {
        Mockito.when(repository.findByName(any())).thenReturn(Optional.empty());

        FrameworkFilter filter = new FrameworkFilter("Test1", null, null,null);

        ApiException result = Assertions.assertThrows(ApiException.class, () ->
                service.getFrameworkByFilter(filter)
        );

        Assertions.assertEquals(result.getMessage(), "Framework not found");
        Assertions.assertEquals(result.getStatus(), HttpStatus.NOT_FOUND);
    }

    @Test
    public void testGetFrameworkByFilter_HypeLevel() {
        Mockito.when(repository.findByHypeLevel(any()))
                .thenReturn(TestUtils.getTestData()
                        .stream()
                        .filter(javaScriptFramework -> javaScriptFramework.getHypeLevel() == HypeLevel.LOW)
                        .collect(Collectors.toList()));

        FrameworkFilter filter = new FrameworkFilter(null, HypeLevel.LOW, null,null);

        Object result = service.getFrameworkByFilter(filter);

        Assertions.assertTrue(result instanceof List);
        Assertions.assertTrue(!((List<?>) result).isEmpty());
    }

    @Test
    public void testGetFrameworkByFilter_DeprecationDateBefore() {
        Mockito.when(repository.findByDeprecationDateIsBefore(any()))
                .thenReturn(TestUtils.getTestData()
                        .stream()
                        .filter(javaScriptFramework -> javaScriptFramework.getDeprecationDate()
                                .isBefore(LocalDate.of(2001, 12,12)))
                        .collect(Collectors.toList()));

        FrameworkFilter filter = new FrameworkFilter(null, null, LocalDate.of(2001, 12,12),null);

        Object result = service.getFrameworkByFilter(filter);

        Assertions.assertTrue(result instanceof List);
        Assertions.assertTrue(!((List<?>) result).isEmpty());
    }

    @Test
    public void testGetFrameworkByFilter_DeprecationDateAfter() {
        Mockito.when(repository.findByDeprecationDateIsAfter(any()))
                .thenReturn(TestUtils.getTestData()
                        .stream()
                        .filter(javaScriptFramework -> javaScriptFramework.getDeprecationDate()
                                .isAfter(LocalDate.of(2000, 12,12)))
                        .collect(Collectors.toList()));

        FrameworkFilter filter = new FrameworkFilter(null, null, null,LocalDate.of(2000, 12,12));

        Object result = service.getFrameworkByFilter(filter);

        Assertions.assertTrue(result instanceof List);
        Assertions.assertTrue(!((List<?>) result).isEmpty());
    }

    @Test
    public void testGetFrameworkByFilter_NullFilter() {
        FrameworkFilter filter = new FrameworkFilter(null, null, null,null);

        ApiException result = Assertions.assertThrows(ApiException.class, () ->
                service.getFrameworkByFilter(filter)
        );

        Assertions.assertEquals(result.getMessage(), "No filter provided");
        Assertions.assertEquals(result.getStatus(), HttpStatus.BAD_REQUEST);
    }


}
