package com.etnetera.hr.service;

import com.etnetera.hr.data.FrameworkFilter;
import com.etnetera.hr.data.JavaScriptFramework;

public interface JavaScriptFrameworkService {

    Iterable<JavaScriptFramework> getAllFrameworks();

    JavaScriptFramework getFrameworkById(Long id);

    Object getFrameworkByFilter(FrameworkFilter filter);

    JavaScriptFramework createFramework(JavaScriptFramework newFramework);

    JavaScriptFramework updateFramework(Long id,JavaScriptFramework updatedFramework);

    void deleteFramework(Long id);
}
