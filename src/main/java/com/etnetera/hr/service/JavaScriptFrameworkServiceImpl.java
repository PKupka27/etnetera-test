package com.etnetera.hr.service;

import com.etnetera.hr.data.ApiException;
import com.etnetera.hr.data.FrameworkFilter;
import com.etnetera.hr.data.JavaScriptFramework;
import com.etnetera.hr.data.Messages;
import com.etnetera.hr.repository.JavaScriptFrameworkRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class JavaScriptFrameworkServiceImpl implements JavaScriptFrameworkService {

    private final JavaScriptFrameworkRepository repository;

    public JavaScriptFrameworkServiceImpl(JavaScriptFrameworkRepository repository) {
        this.repository = repository;
    }

    @Override
    public Iterable<JavaScriptFramework> getAllFrameworks() {
        return repository.findAll();
    }

    @Override
    public JavaScriptFramework getFrameworkById(Long id) {
        return repository.findById(id).orElseThrow(() -> {
            throw new ApiException(HttpStatus.NOT_FOUND, Messages.NOT_FOUND_MESSAGE);
        });
    }

    @Override
    public Object getFrameworkByFilter(FrameworkFilter filter) {
        if (Objects.nonNull(filter.getName())) {
            return repository.findByName(filter.getName()).orElseThrow(() -> {
                throw new ApiException(HttpStatus.NOT_FOUND, Messages.NOT_FOUND_MESSAGE);
            });
        } else if (Objects.nonNull(filter.getHypeLevel())) {
            return repository.findByHypeLevel(filter.getHypeLevel());
        } else if (Objects.nonNull(filter.getDeprecationDateBefore())) {
            return repository.findByDeprecationDateIsBefore(filter.getDeprecationDateBefore());
        } else if (Objects.nonNull(filter.getDeprecationDateAfter())) {
            return repository.findByDeprecationDateIsAfter(filter.getDeprecationDateAfter());
        } else {
            throw new ApiException(HttpStatus.BAD_REQUEST, Messages.NO_FILTER);
        }
    }

    @Override
    public JavaScriptFramework createFramework(JavaScriptFramework newFramework) {
        return repository.save(newFramework);
    }

    @Override
    public JavaScriptFramework updateFramework(Long id, JavaScriptFramework updatedFramework) {
        return repository.findById(id)
                .map(frameworkToUpdate -> {
                    frameworkToUpdate.setName(updatedFramework.getName());
                    frameworkToUpdate.setVersion(updatedFramework.getVersion());
                    frameworkToUpdate.setDeprecationDate(updatedFramework.getDeprecationDate());
                    frameworkToUpdate.setHypeLevel(updatedFramework.getHypeLevel());
                    return repository.save(frameworkToUpdate);
                }).orElseThrow(() -> new ApiException(HttpStatus.NOT_FOUND, Messages.NOT_FOUND_MESSAGE));
    }

    @Override
    public void deleteFramework(Long id) {
        JavaScriptFramework frameworkToDelete = repository.findById(id).orElseThrow(() -> {
            throw new ApiException(HttpStatus.NOT_FOUND, Messages.NOT_FOUND_MESSAGE);
        });
        repository.delete(frameworkToDelete);
    }
}
