package com.etnetera.hr.controller;

import com.etnetera.hr.data.FrameworkFilter;
import com.etnetera.hr.data.JavaScriptFramework;
import com.etnetera.hr.service.JavaScriptFrameworkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Simple REST controller for accessing application logic.
 *
 * @author Etnetera
 */
@RestController
@RequestMapping("/frameworks")
public class JavaScriptFrameworkController {

    private final JavaScriptFrameworkService service;

    @Autowired
    public JavaScriptFrameworkController(JavaScriptFrameworkService service) {
        this.service = service;
    }

    @GetMapping
    public ResponseEntity<Iterable<JavaScriptFramework>> getAllFrameworks() {
        return ResponseEntity.ok(service.getAllFrameworks());
    }

    @GetMapping("/{id}")
    public ResponseEntity<JavaScriptFramework> getFrameworkById(@PathVariable Long id) {
        return ResponseEntity.ok(service.getFrameworkById(id));
    }

    @PostMapping("/filter")
    public ResponseEntity getFrameworkByName(@RequestBody FrameworkFilter filter) {
        return ResponseEntity.ok(service.getFrameworkByFilter(filter));
    }

    @PostMapping
    public ResponseEntity<JavaScriptFramework> createFramework(@RequestBody JavaScriptFramework newFramework) {
        return ResponseEntity.ok(service.createFramework(newFramework));
    }

    @PutMapping("/{id}")
    public ResponseEntity<JavaScriptFramework> updateFramework(@PathVariable Long id, @RequestBody JavaScriptFramework updatedFramework) {
		return ResponseEntity.ok(service.updateFramework(id, updatedFramework));
    }

    @DeleteMapping("/{id}")
    public void deleteFramework(@PathVariable Long id) {
        service.deleteFramework(id);
    }

}
