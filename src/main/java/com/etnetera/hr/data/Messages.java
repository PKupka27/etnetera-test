package com.etnetera.hr.data;

public class Messages {
    public static final String NOT_FOUND_MESSAGE = "Framework not found";
    public static final String NO_FILTER = "No filter provided";
}
