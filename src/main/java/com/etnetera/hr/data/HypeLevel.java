package com.etnetera.hr.data;

import com.fasterxml.jackson.annotation.JsonValue;

public enum HypeLevel {
    LOW("Low"),
    MEDIUM("Medium"),
    HIGH("High");

    HypeLevel(String value) {
        this.value = value;
    }

    private String value;

    @JsonValue
    public String getValue() {
        return value;
    }
}
