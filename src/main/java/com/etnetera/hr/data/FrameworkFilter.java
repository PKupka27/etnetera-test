package com.etnetera.hr.data;

import java.time.LocalDate;

public class FrameworkFilter {

    private String name;

    private HypeLevel hypeLevel;

    private LocalDate deprecationDateBefore;

    private LocalDate deprecationDateAfter;

    public FrameworkFilter(String name, HypeLevel hypeLevel, LocalDate deprecationDateBefore, LocalDate deprecationDateAfter) {
        this.name = name;
        this.hypeLevel = hypeLevel;
        this.deprecationDateBefore = deprecationDateBefore;
        this.deprecationDateAfter = deprecationDateAfter;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public HypeLevel getHypeLevel() {
        return hypeLevel;
    }

    public void setHypeLevel(HypeLevel hypeLevel) {
        this.hypeLevel = hypeLevel;
    }

    public LocalDate getDeprecationDateBefore() {
        return deprecationDateBefore;
    }

    public void setDeprecationDateBefore(LocalDate deprecationDateBefore) {
        this.deprecationDateBefore = deprecationDateBefore;
    }

    public LocalDate getDeprecationDateAfter() {
        return deprecationDateAfter;
    }

    public void setDeprecationDateAfter(LocalDate deprecationDateAfter) {
        this.deprecationDateAfter = deprecationDateAfter;
    }
}
